"use strict";
class naturalNumber{
    constructor(n){
        this.n = n;
        this.n2 = n*n;
    }
    find3(){
        var n2 = this.n2;
        while(n2>1){
            if(n2%10==3){
                console.log('found 3 in n^2 ');
                break;
            }
            n2 = parseInt(n2/10);
        }
    }
    reverseN(){
        var result = 0;
        var n = this.n;
        while(n>0){
            result = result*10 + n%10;
            n = parseInt(n/10);
        }
        console.log(result);
        }
}
let a = new naturalNumber(5356);
a.find3();
a.reverseN();
