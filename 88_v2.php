<?php
class naturalNumber{
    private function __construct($n){
        $this->n = $n;
    }
    public function find3(){
        $n2 = $this->n*$this->n;
        while($n2>1){
		if($n2%10==3){
            echo('found 3 in n^2 <br>');
            break;
			}
			   $n2 = (int)($n2/10);
        }
    }
    public function reverseN(){
        $number = $this->n;
        $result =0;
        while($number>0){
            $result = $result*10 + $number%10;
            $number = (int)($number/10);
        }
        echo($result);
    }
}

$a = new naturalNumber(152);
$a->find3();
$a->reverseN();
?>