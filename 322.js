var dividersSum = function(){
    var maxSum = 2; //Each number has at least 2 dividers: 1 and self
    var maxNumber = 1;
    for(var i = 1; i<=10000;i++){
        var currentSum = 0;
        for(j=1;j<i;j++){
            if(i%j==0){
                currentSum = currentSum + j;
            }
        }
        if(currentSum>maxSum){
            maxSum = currentSum;
            maxNumber = i;
        }
    }
    return(maxNumber);
}
console.log(dividersSum());